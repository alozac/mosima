Implémentation du modèle de confiance et de réputation FIRE, décrit dans “Developing an Integrated Trust and Reputation Model for Open Multi-Agent Systems”, Huynh, Trung Dong & R. Jennings, Nicholas & R. Shadbolt, Nigel. (2006). 13. 119-154. Une version complète et une version résuée de l'article sont disponibles sous ressources/FIRE_full_article et ressources/FIRE_short_article.

Ce projet a été réalisé dans le cadre d'un projet universitaire pour l'UE Modélisation et Simulation multi-agent de la filière M2 ANDROIDE 2018-2019 de Sorbonne Université. Le modèle est implémenté en Netlogo, le code se trouvant dans FIRE.nlogo3d. 

Le sujet de ce projet est disponible sous ressources/Sujet_Projet_SMA.html, et notre implémentation est décrite et analysée sous ressources/rapport.pdf. 
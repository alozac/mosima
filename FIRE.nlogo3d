extensions[ profiler ]
globals [
  world_radius

  INTERACTION WITNESS CERTIFIED ;; enum of components names

  FIRE-IT FIRE-IT-WR FIRE-IT-CR FIRE-IT-WR-CR NOTRUST ;; enum of trust models

  GOOD OK BAD INTERMITTENT                 ;; enum of providers types
  PL_PERFECT PL_GOOD PL_OK PL_BAD PL_WORST ;; performance levels
  std_dev_good std_dev_ok std_dev_bad      ;; standard deviations for distribution of providers types

  totalProviders ;; Total number of providers
  totalConsumers ;; Total number of consumers

  meanRhoI meanRhoW meanRhoC ;; mean of reliability factors
  nbTrustsComputed           ;; used to compute the mean of reliability factors

  H ;; rating-history-size = max nb of ratings/references stored
  gamma_I gamma_W gamma_C ;;  reliability functions parameters
  lambda ;; used for the IT's rating weight function - IT's recency scaling factor

  nBF nRL  ;; branching factor and referral length threshold, used for Witness Reputation

  pCPC pPPC ;; population change limits (consumers and providers)

  pCLC pPLC ;; proba for a consumer, resp. provider, to change location
  deltaPos  ;; angular location changes in [-deltaPos, +deltaPos]

  M              ;; a provider changes its average performance by an amount selected in [-M, M]
  pMuC           ;; proba that a provider changes its average performance
  pProfileSwitch ;; proba that a provider switches to an other profile

  temp_decrease_rate

  meanUG-FIRE-IT
  meanUG-FIRE-IT-WR
  meanUG-FIRE-IT-CR
  meanUG-FIRE-IT-WR-CR
  meanUG-NOTRUST

  totalMeanUG    ;; equals to the total of meanUGs obtained for the complete FIRE model (i.e. the mean of all meanUG-FIRE-IT-WR-CR)
  UGPerformances ;; used to store the overall UG performances for each iteration for a sensibility analysis
  currIter       ;; current iteration for a sensibility analysis

  time ; only used to plot the time spent by each iteration
]

breed [ consumers consumer ]
breed [ providers provider ]

turtles-own [
  rad       ;; vision radius
  theta phi ;; polar coordinations
  liar?     ;; indicates whether the agent ca lie or not
]
consumers-own [
  model             ;; the trust model used for the provider selection
  activity-level    ;; proba consumer needs to use service
  ratings           ;; list of (b,v,t) = rating of value v in [-1, 1] given TO provider b at time t (in ticks)
  acquaintances     ;; list of agents located in the radius of vision
  temperature       ;; set to decrease overtime to decrease exploration
  UG                ;; last UG computed
  total_UG_observed ;; sum of all UG computed since the initialization of the consumer
  nbInteractions    ;; number of past interactions
]
providers-own [
  perf_type  ;; performance type of the provider
  mu         ;; mean used to get a quality of service randomly
  sigma      ;; deviation used to get a quality of service randomly
  references ;; list of (a,v,t) = rating of value v in [-1, 1] given BY consumer a at time t (in ticks)
]

;;;;;;;;;;;;;;;;;;;;;;
;; Setup Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;

; Initialize all the cnstantsand agents in the environment
to setup [ isFstSetup ]
  ; Since setup is called between each iteration for a sensibility analysis,
  ; we need to store temporarily the values related to the analysis before calling clear-all
  let tmpUGPerf UGPerformances
  let tmpCurrIter currIter
  clear-all
  ifelse isFstSetup [
    set currIter 0
    set UGPerformances []
  ][
    set UGPerformances tmpUGPerf
    set currIter tmpCurrIter
  ]

  initialize_constants
  initialize-agents
  update_acquaintances

  reset-ticks
end

to initialize-agents
  if active-FIRE-IT       [ init-consumers nb-consumers-per-group FIRE-IT ]
  if active-FIRE-IT-WR    [ init-consumers nb-consumers-per-group FIRE-IT-WR ]
  if active-FIRE-IT-CR    [ init-consumers nb-consumers-per-group FIRE-IT-CR ]
  if active-FIRE-IT-WR-CR [ init-consumers nb-consumers-per-group FIRE-IT-WR-CR ]
  if active-NOTRUST       [ init-consumers nb-consumers-per-group NOTRUST ]

  init-providers nb-providers-ok OK
  init-providers nb-providers-good GOOD
  init-providers nb-providers-bad BAD
  init-providers nb-providers-intermittent INTERMITTENT
end

to init-consumers [ nb modelType ]
  create-consumers nb [
    set model modelType
    set activity-level random-interval 0.25 1 ;; proba of needing a service in [0.25, 1]
    set ratings []
    set temperature initial-temperature
    set total_UG_observed 0
    set UG 0
    set nbInteractions 0
    set color green
    init-position
    ifelse random-float 1 < liars_proportion / 100
    [ set liar? true ][ set liar? false ]
  ]
end

to init-providers [ nb typeP ]
  create-providers nb [
    set references []
    set perf_type typeP
    init_performance_params
    set color red
    init-position
    ifelse random-float 1 < liars_proportion / 100
    [ set liar? true ][ set liar? false ]
  ]
end

to initialize_constants
  set FIRE-IT       0
  set FIRE-IT-WR    1
  set FIRE-IT-CR    2
  set FIRE-IT-WR-CR 3
  set NOTRUST       4

  set INTERACTION 0
  set WITNESS     1
  set CERTIFIED   2

  set GOOD 0
  set OK   1
  set BAD  2
  set INTERMITTENT 3

  set PL_PERFECT 10
  set PL_GOOD    5
  set PL_OK      0
  set PL_BAD     -5
  set PL_WORST   -10

  set std_dev_good 1
  set std_dev_ok   2
  set std_dev_bad  2

  set gamma_I ( - ln 0.5 )
  set gamma_W ( - ln 0.5 )
  set gamma_C ( - ln 0.5 )
  set lambda  ( - 5 / ln 0.5 )

  set nBF 2
  set nRL 5

  set pCPC 0.05
  set pPPC 0.02

  set pCLC 0.10
  set pPLC 0.10
  set deltaPos pi / 20

  set M 1
  set pMuC 0.1
  set pProfileSwitch 0.02

  set H 10
  set temp_decrease_rate 1
  set world_radius 1
  set totalProviders nb-providers-good + nb-providers-ok + nb-providers-bad + nb-providers-intermittent
  let models filter [ g -> g ] (list active-FIRE-IT active-FIRE-IT-WR active-FIRE-IT-CR active-FIRE-IT-WR-CR active-NOTRUST )
  set totalConsumers nb-consumers-per-group * (length models )
end


;;;;;;;;;;;;;;;;;;;;;;;;
;; Runtime Procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;

to go
  if ticks = NB_ROUNDS [
    set UGPerformances lput ( totalMeanUG / NB_ROUNDS ) UGPerformances
    set currIter ( currIter + 1 )
    ifelse currIter = nb_iterations
      [ stop ]
      [ setup false]
  ]

  ;profiler:start
  reset-timer

  set meanRhoI 0
  set meanRhoW 0
  set meanRhoC 0
  set nbTrustsComputed 0
  ask consumers [
    ;; if the consumer wants to use the service
    if random-float 1 < activity-level [
      let chosenProvider nobody
      ; then he chooses a provider according to its trust model and interact with him
      ifelse model = NOTRUST [
        set chosenProvider select-provider-NOTRUST
      ][
        set chosenProvider select-provider-FIRE
      ]
      interact chosenProvider
    ]
  ]

  ; Update of variables used for plotting
  if active-FIRE-IT       [ set meanUG-FIRE-IT       mean [UG] of consumers with [ model = FIRE-IT ] ]
  if active-FIRE-IT-WR    [ set meanUG-FIRE-IT-WR    mean [UG] of consumers with [ model = FIRE-IT-WR ] ]
  if active-FIRE-IT-CR    [ set meanUG-FIRE-IT-CR    mean [UG] of consumers with [ model = FIRE-IT-CR ] ]
  if active-FIRE-IT-WR-CR [ set meanUG-FIRE-IT-WR-CR mean [UG] of consumers with [ model = FIRE-IT-WR-CR ] ]
  if active-NOTRUST       [ set meanUG-NOTRUST       mean [UG] of consumers with [ model = NOTRUST ] ]
  set totalMeanUG ( totalMeanUG + meanUG-FIRE-IT-WR-CR )

  if nbTrustsComputed != 0 [
    set meanRhoI ( meanRhoI / nbTrustsComputed )
    set meanRhoW ( meanRhoW / nbTrustsComputed )
    set meanRhoC ( meanRhoC / nbTrustsComputed )
  ]
  set time timer


  ; Dynamic changes
  if active_population_changes [ changePopulation ]
  if active_location_changes [ update_location ]
  if active_behaviour_changes [ changeProvidersBehaviour ]
  if active_population_changes or active_location_changes or active_behaviour_changes
    [ update_acquaintances ]

  ;profiler:stop
  ;print profiler:report
  ;profiler:reset
  tick
end


;; Choose a provider according to FIRE model
to-report select-provider-FIRE ;; consumer procedure
  let chosenProvider nobody
  let noTrustValue []
  let bestProvider nobody
  let bestProviderTrust -2

  ;; foreach provider, we calculate his trustworthiness from the ratings obtained according to each component' rules
  let nearbyProviders ( [ self ] of acquaintances with [ breed = providers ] )
  foreach nearbyProviders [ p ->
    let ratingsIT []
    if model = FIRE-IT or model = FIRE-IT-WR or model = FIRE-IT-CR or model = FIRE-IT-WR-CR
      [ set ratingsIT ratings_set_IT p ]
    let ratingsWR []
    if model = FIRE-IT-WR or model = FIRE-IT-WR-CR
      [ set ratingsWR ratings_set_WR p ]
    let ratingsCR []
    if model = FIRE-IT-CR or model = FIRE-IT-WR-CR
      [ set ratingsCR ratings_set_CR p ]
    ifelse empty? ratingsIT and empty? ratingsWR and empty? ratingsCR [
      ; We keep in memory unkown providers...
      set noTrustValue lput p noTrustValue
    ]
    [
      ;; Or we evaluate his trustworthiness
      let trustworthiness compute_trust_value ratingsIT ratingsWR ratingsCR

      ;; and we only keep the provider with higher trust value
      if trustworthiness > bestProviderTrust [
        set bestProviderTrust trustworthiness
        set bestProvider p
      ]
    ]
  ]


  ;; Choose best provider or explore (p138)
  ifelse bestProvider = nobody
  [
    ;; if nobody has trust value, choose randomly in noTrustValue
    if not empty? noTrustValue [
      set chosenProvider one-of noTrustValue ]
  ]
  [ ifelse empty? noTrustValue
    [
      ;; if every close providers have been explored, choose best provider
      set chosenProvider bestProvider
    ]
    [
      ;; Botzman strategy (p138) to choose between exploit (a1) and explore (a2)
      let ER_a1 bestProviderTrust
      let ER_a2 0
      if nbInteractions != 0 [ set ER_a2 (total_UG_observed / nbInteractions) ]

      let e_a1 (exp (ER_a1 / temperature))
      let e_a2 (exp (ER_a2 / temperature))
      let prob_a1 (e_a1 / (e_a1 + e_a2))
      ifelse random-float 1 <= prob_a1
      [
        set chosenProvider bestProvider ]
      [
        set chosenProvider one-of noTrustValue
        set temperature max list 1 (temperature - temp_decrease_rate)
      ]
    ]
  ]
  report chosenProvider
end

; get a list of ratings according to the IT component rules
to-report ratings_set_IT [ p ] ;; consumer procedure
  report filter [r -> get_agent r = p] ratings
end

; get a list of ratings according to the WR component rules
to-report ratings_set_WR [ p ] ;; consumer procedure
  let ratings_set []
  let threshold nRL
  let nbWitnesses nBF
  ; get the clients in the neighbourhood
  let acqC acquaintances with [ breed = consumers and model = [ model ] of myself]
  ; Pick randomly 2 potential witnesses among the neighbours
  let potential_witnesses n-of ( min list count acqC nBF) ( acqC )

  ; Search for witnesses until the search reach the referral length threshold or until we have enough witnesses
  while [ threshold > 0 and nbWitnesses > 0 ] [
    let tmp_pot_witnesses no-turtles
    ; we ask ratings or other potential witnesses to each potential witnesses
    ask potential_witnesses [
      if nbWitnesses > 0 [
        ; rateref = (isWitness, list), with isWitness a boolean, and list a list of agents or ratings
        let rateref get_ratings_or_referrals p
        ;; if true, the consumer has indeed had interactions with p, and has returned his ratings
        ifelse item 0 rateref [
          set ratings_set sentence ratings_set ( item 1 rateref )
          set nbWitnesses ( nbWitnesses - 1 )
        ]
        ;; else, he has returned potential witnesses from his acquaintances
        [
          set tmp_pot_witnesses ( turtle-set tmp_pot_witnesses ( item 1 rateref ) )
        ]
      ]
    ]
    set potential_witnesses tmp_pot_witnesses
    set threshold (threshold - 1)
  ]
  report ratings_set
end

;; return a couple (isWitness, rate_ref) with:
;; - isWitness a boolean indicating whether or not the current consumer is a witness (has ratings of p or not)
;; - rate_ref a list of ratings if isWitness=true, else a list of nBF potential witnesses picked from his acquaintances
to-report get_ratings_or_referrals [ p ]  ;; consumer procedure
  let ratings_p filter [ r -> item 0 r = p ] ratings
  ifelse empty? ratings_p [
    let acqC acquaintances with [ breed = consumers ]
    ; A liar will report only 1 potential witness, instead of nBF
    ifelse liar? [ report list false ( n-of ( min list count acqC 1 ) acqC  ) ]
    [ report list false ( n-of ( min list count acqC nBF ) acqC  ) ]
  ][
    ifelse liar?  [
      let modified_ratings []
      ; A liar will modify each rating, either inflating or deflating the value
      foreach ratings_p [ r ->
        ifelse random-float 1 < 0.5
        [ set modified_ratings lput inflate_rating r modified_ratings ]
        [ set modified_ratings lput deflate_rating r modified_ratings ]
      ]
      report list true modified_ratings
    ]
    [ report list true ratings_p ]
  ]
end

; get a list of ratings according to the CR component rules
to-report ratings_set_CR [ p ] ;; consumer procedure
  ifelse liar?
  ; First version, see definition of inflate_rating
  ; [ report map inflate_rating ( [references] of p ) ]
  ; Second version, see definition of inverse_negative_rating
  ; [ report map inverse_negative_rating ( [references] of p ) ]
  ; Third version
  [ report map set_value_to_maximum ( [references] of p ) ]
  [ report [ references ] of p ]
end


;; Choose provider without trust model, i.e. randomly
to-report select-provider-NOTRUST ; consumer procedure
  let nearbyProviders ( acquaintances with [ breed = providers ] )
  ifelse any? nearbyProviders [
    report one-of nearbyProviders
  ][ report nobody ]
end

;; Use the service of chosenProvider and rate him
to interact [ chosenProvider ] ;; consumer procedure
  if chosenProvider != nobody [
    ; UG = quality of chosenProvider's service
    set UG [getPerformance myself] of chosenProvider
    ;; Store the rating, i.e. the performance normalized to the range of [-1, 1]
    add-rating chosenProvider ( UG / 10 )

    ;; Make the provider know about the rating to allow him to show it to potential consumers as a reference (CR component)
    ask chosenProvider [ add-reference myself ]

    ; Used for Bolzman strategy (see select-provider-FIRE)
    set total_UG_observed (total_UG_observed + UG)
    set nbInteractions ( nbInteractions + 1)
  ]
end

; Compute the trust value of a provider according to the ratings of each component
to-report compute_trust_value [ ratings_setIT ratings_setWR ratings_setCR ]
  let rhoI 0
  let rhoW 0
  let rhoC 0
  ;; Trust value for IT component
  let T_I 0
  let wI 0
  if active-FIRE-IT or active-FIRE-IT-WR or active-FIRE-IT-CR or active-FIRE-IT-WR-CR [
    let weightsRatings sum ( map [ r -> ( get_value r ) * ( weight_function_IT r ) ] ratings_setIT )
    let allWeights sum ( map weight_function_IT ratings_setIT )
    if allWeights = 0 [ set allWeights 1 ]
    set T_I weightsRatings / allWeights
    set rhoI reliability INTERACTION T_I ratings_setIT allWeights
    set wI W_IT * rhoI
  ]

  ;; Trust value for WR component
  let T_W 0
  let wW 0
  if active-FIRE-IT-WR or active-FIRE-IT-WR-CR [
    let weightsRatings sum ( map [ r -> ( get_value r ) * ( weight_function_WR r ) ] ratings_setWR )
    let allWeights sum ( map weight_function_WR ratings_setWR )
    if allWeights = 0 [ set allWeights 1 ]
    set T_W weightsRatings / allWeights
    set rhoW reliability WITNESS T_W ratings_setWR allWeights
    set wW W_WR * rhoW
  ]

  ;; Trust value for CR component
  let T_C 0
  let wC 0
  if active-FIRE-IT-CR or active-FIRE-IT-WR-CR [
    let weightsRatings sum ( map [ r -> ( get_value r ) * ( weight_function_CR r ) ] ratings_setCR )
    let allWeights sum ( map weight_function_CR ratings_setCR )
    if allWeights = 0 [ set allWeights 1 ]
    set T_C weightsRatings / allWeights
    set rhoC reliability CERTIFIED T_C ratings_setCR allWeights
    set wC W_CR * rhoC
  ]

  set meanRhoI ( meanRhoI + rhoI )
  set meanRhoW ( meanRhoW + rhoW )
  set meanRhoC ( meanRhoC + rhoC )
  set nbTrustsComputed ( nbTrustsComputed + 1 )

  ;; Overall trust formula
  ifelse wI + wW + wC != 0 [
    report ( T_I * wI + T_W * wW + T_C * wC ) / ( wI + wW + wC )
  ][ report 0 ]
end

; Compute the reliability factor for the component "component"
to-report reliability [ component trust_value ratings_set all_weights]
  let gamma 0
  let dev_tmp 0

  ifelse component = INTERACTION [
    set gamma gamma_I
    set dev_tmp  sum ( map [ r -> abs ( get_value r - trust_value) * ( weight_function_IT r ) ] ratings_set)
  ][ ifelse component = WITNESS [
    set gamma gamma_W
    set dev_tmp  sum ( map [ r -> abs ( get_value r - trust_value) * ( weight_function_WR r ) ] ratings_set)
  ][
    set gamma gamma_C
    set dev_tmp  sum ( map [ r -> abs ( get_value r - trust_value) * ( weight_function_CR r ) ] ratings_set)
  ]]

  let rating_reliability    1 - exp ( - gamma * all_weights )
  let deviation_reliability 1 - 1 / 2 * dev_tmp / all_weights
  report rating_reliability * deviation_reliability
end



to-report weight_function_IT [ rating ]
  report exp ( - ( ticks - get_time rating )  / lambda )
end

; According to the description of WR component, is equal to weight_function_IT
to-report weight_function_WR [ rating ]
  report weight_function_IT rating
end

; According to the description of WR component, is equal to weight_function_IT
to-report weight_function_CR [ rating ]
  report weight_function_IT rating
end



;;
;; Procedures for dynamic environnment
;;


;; Each round, pPPC% of providers and pCPC% of consumers die and are replaced by others
;; The proportions of providers of different proﬁles and that of consumers in different groups are maintained
to changePopulation
  let nbProviders round random-float ( pPPC * totalProviders )
  let provToDie n-of nbProviders providers
  foreach [self] of provToDie [ p ->
    init-providers 1 [ perf_type ] of p
  ]
  ask provToDie [ die ]

  let nbConsumers round random-float ( pCPC * totalConsumers )
  let consToDie n-of nbConsumers consumers
  foreach [self] of consToDie [ c ->
    init-consumers 1 [ model ] of c
  ]
  ask consToDie [ die ]
end

;; Each agent may move after each round with proba of pPLC (providers) or pCLC (consumers)
to update_location
  ask providers [
   if random-float 1 < pPLC
    [
      set phi phi +  random-float (2 * deltaPos) - deltaPos
      set theta theta +  random-float (2 * deltaPos) - deltaPos
      calculate_xyz_position
    ]
  ]
  ask consumers [
    if random-float 1 < pCLC
    [
      set phi phi +  random-float (2 * deltaPos) - deltaPos
      set theta theta +  random-float (2 * deltaPos) - deltaPos
      calculate_xyz_position
    ]
  ]
end

;; After each round, the average performance of a provider may change with a proba of pMuC by an amount selected in [−M,+M],
;; and a provider can switch to a different proﬁle with a proba of pProﬁleSwitch.
to changeProvidersBehaviour
  ask providers [
    if perf_type != INTERMITTENT and random-float 1 < pMuC [
      set mu ( mu + random-interval (- M) M )
      ifelse perf_type = GOOD [
        set mu min list mu PL_PERFECT
        set mu max list mu PL_GOOD
      ][ ifelse perf_type = OK [
        set mu min list mu PL_GOOD
        set mu max list mu PL_OK
      ][
        set mu min list mu PL_OK
        set mu max list mu PL_WORST
      ]]
    ]
    if random-float 1 < pProfileSwitch [
      set perf_type one-of ( list GOOD OK BAD INTERMITTENT )
      init_performance_params
    ]
  ]
end

;; After all dynamic changes, we have to update the neighbourhood of each consumer
to update_acquaintances
  ask consumers [
    set acquaintances turtles in-radius rad
  ]
end



;;;;;;;;;;;;;;;;;;;;;;;;
;; Utility procedures ;;
;;;;;;;;;;;;;;;;;;;;;;;;

; Get the quality of service of a provider
to-report getPerformance [ consumer_service ] ;; provider procedure
  let perf 0
  ifelse perf_type = INTERMITTENT
  [ set perf random-interval PL_BAD PL_GOOD ]
  [ set perf random-normal mu sigma ]

  ;; degrade performance linearly if consumer located outside of provider's operational range
  ;; Here, if perf=10 and consumer and provider located at the opposite end of the sphere from each other (dist=2*world-radius),
  ;; then perf = -10
  let dist distance consumer_service - rad
  if dist > 0 [
    let degradation 20 / ( 2 * world_radius) * dist
    set perf ( perf - degradation )
  ]
  if perf > 10 [ set perf 10 ]
  if perf < -10 [ set perf -10 ]
  report perf
end

; Reports a rating with a value inversed if the original value is negative
to-report inverse_negative_rating [ r ]
  ifelse get_value r < 0 [
    report (list get_agent r (- get_value r) get_time r)
  ][
     report r
  ]
end

;Reports a rating with a value increased by a number in [0, f(x)=-0.5*x+0.5]
; So, the worse the rating, the greater the inflation can be
to-report inflate_rating [ r ]
  let max_inflation ( -0.5 * get_value r + 0.5)
  let inflated_value get_value r + random-interval 0 max_inflation
  report (list get_agent r inflated_value get_time r)
end

;Reports a rating with a value decreased by a number in [0, f(x)= 0.5*x+0.5]
to-report deflate_rating [ r ]
  let max_deflation ( 0.5 * get_value r + 0.5)
  let deflated_value get_value r - random-interval 0 max_deflation
  report (list get_agent r deflated_value get_time r)
end

; Reports a rating with a value of 1 (the maximum value possible)
to-report set_value_to_maximum [ r ]
  report (list get_agent r 1 get_time r)
end

;; Add a rating of provider "agent" to the list of ratings of the current consumer
to add-rating [agent value] ;; consumer procedure
  set ratings fput (list agent value ticks) ratings ;; add rating at the beginning of the list
  if length ratings > H ;; we don't keep last rating if list full
  [ set ratings but-last ratings ] ;;
end

;; Add a rating given by consumer "agent" to the list of references of the current provider
;; The value of the rating is the UG of the consumer normalized to [-1, 1]
to add-reference [ agent ] ;; provider procedure
  ;; we keep the rating as a reference only if it's a positive rating
  let value [ UG / 10 ] of agent
  if value > 0 [
    ;; we only keep the H best references
    set references fput (list agent value ticks) references
    set references sort-by [ [ r1 r2 ] -> get_value r1 < get_value r2 ] references
    if length references > H
    [ set references but-last references ] ;;
  ]
end

;; reports the agent rated
to-report get_agent [rating]
  report item 0 rating
end

;; reports the value of a rating
to-report get_value [rating]
  report item 1 rating
end

;; reports the time (in ticks) of a rating
to-report get_time [rating]
  report item 2 rating
end

;; init mean and standard deviations of a provider according to its perf_type
to init_performance_params ;; provider procedure
  ifelse perf_type = GOOD [
    set mu random-interval PL_GOOD PL_PERFECT
    set sigma std_dev_good
  ][ ifelse perf_type = OK [
    set mu random-interval PL_OK PL_GOOD
    set sigma std_dev_ok
  ][
    set mu random-interval PL_WORST PL_OK
    set sigma std_dev_bad
  ]]
end

to init-position ;; turtle procedure
  ;; position the agents on the spherical world
  set theta 2 * pi * (random-float 60)
  set phi (acos(2 * (random-float 1) - 1))
  calculate_xyz_position
  set size 0.1
  set shape "dot"
  set rad ( world_radius * random-interval min-radius max-radius ) ;; radius of operation : see if a specific value is given in paper
end

;; set (x,y,z) position from (phi, theta) coordinates
to calculate_xyz_position ;; turtle procedure
  let x (sin phi * cos theta * world_radius)
  let y (sin phi * sin theta * world_radius)
  let z (cos phi * world_radius)
  setxyz x y z
end

to-report random-interval [ mini maxi ]
  report random-float (maxi - mini) + mini
end
@#$#@#$#@
GRAPHICS-WINDOW
0
0
47
48
-1
-1
13.0
1
10
1
1
1
0
1
1
1
-1
1
-1
1
-1
1
1
0
1
ticks
30.0

BUTTON
157
341
236
374
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

BUTTON
70
341
148
374
setup
setup true
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
190
19
377
52
nb-consumers-per-group
nb-consumers-per-group
0
500
500.0
1
1
NIL
HORIZONTAL

SLIDER
6
19
183
52
nb-providers-good
nb-providers-good
0
100
10.0
1
1
NIL
HORIZONTAL

SLIDER
16
219
188
252
W_IT
W_IT
0.1
2
2.0
0.1
1
NIL
HORIZONTAL

SLIDER
16
258
188
291
W_WR
W_WR
0.1
2
1.0
0.1
1
NIL
HORIZONTAL

SLIDER
17
297
189
330
W_CR
W_CR
0.1
2
0.5
0.1
1
NIL
HORIZONTAL

PLOT
445
77
1062
329
FIRE performances
Interactions
UG
0.0
100.0
-2.0
7.0
true
true
"" ""
PENS
"FIRE-IT" 1.0 0 -10899396 true "" "plotxy ticks meanUG-FIRE-IT"
"FIRE-IT-WR" 1.0 0 -13345367 true "" "plotxy ticks meanUG-FIRE-IT-WR"
"FIRE-IT-CR" 1.0 0 -955883 true "" "plotxy ticks meanUG-FIRE-IT-CR"
"FIRE-IT-WR-CR" 1.0 0 -2674135 true "" "plotxy ticks meanUG-FIRE-IT-WR-CR"
"NOTRUST" 1.0 0 -16777216 true "" "plotxy ticks meanUG-NOTRUST"

BUTTON
246
341
323
374
go-once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

SLIDER
6
56
184
89
nb-providers-ok
nb-providers-ok
0
100
40.0
1
1
NIL
HORIZONTAL

SLIDER
6
95
184
128
nb-providers-bad
nb-providers-bad
0
100
45.0
1
1
NIL
HORIZONTAL

SLIDER
6
136
186
169
nb-providers-intermittent
nb-providers-intermittent
0
100
5.0
1
1
NIL
HORIZONTAL

MONITOR
202
99
285
144
NB providers
nb-providers-good + nb-providers-ok + nb-providers-bad + nb-providers-intermittent
17
1
11

SLIDER
20
425
192
458
initial-temperature
initial-temperature
1
100
100.0
1
1
NIL
HORIZONTAL

PLOT
437
530
637
680
timer
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot time"

SLIDER
20
386
192
419
NB_ROUNDS
NB_ROUNDS
0
1000
200.0
1
1
NIL
HORIZONTAL

SWITCH
201
386
385
419
active_location_changes
active_location_changes
1
1
-1000

SWITCH
201
425
385
458
active_population_changes
active_population_changes
1
1
-1000

SWITCH
201
465
386
498
active_behaviour_changes
active_behaviour_changes
1
1
-1000

MONITOR
686
19
771
64
good providers
count providers with [ perf_type = GOOD ]
17
1
11

MONITOR
777
20
849
65
ok providers
count providers with [ perf_type = OK ]
17
1
11

MONITOR
855
19
933
64
bad providers
count providers with [ perf_type = BAD ]
17
1
11

MONITOR
940
19
1057
64
intermittent providers
count providers with [ perf_type = INTERMITTENT ]
17
1
11

SWITCH
204
150
367
183
active-NOTRUST
active-NOTRUST
1
1
-1000

SWITCH
205
187
368
220
active-FIRE-IT
active-FIRE-IT
1
1
-1000

SWITCH
206
224
368
257
active-FIRE-IT-WR
active-FIRE-IT-WR
1
1
-1000

SWITCH
207
297
366
330
active-FIRE-IT-WR-CR
active-FIRE-IT-WR-CR
1
1
-1000

MONITOR
316
102
416
147
NIL
totalConsumers
17
1
11

PLOT
717
346
1061
508
Mean of reliability factors
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"meanRhoI" 1.0 0 -5298144 true "" "plot meanRhoI"
"meanRhoW" 1.0 0 -14439633 true "" "plot meanRhoW"
"meanRhoC" 1.0 0 -14070903 true "" "plot meanRhoC"

MONITOR
441
19
679
64
Overall UG performance (FIRE-IT-WR-CR only)
totalMeanUG / ticks
17
1
11

PLOT
447
345
698
507
Consumer's UG distribution
UG
Nb consumers
-10.0
10.0
0.0
10.0
true
true
"" ""
PENS
"FIRE" 1.0 1 -14439633 true "" "histogram [ UG ] of consumers with [ model = FIRE-IT-WR-CR ]"
"NO TRUST" 1.0 1 -14070903 true "" "histogram [ UG ] of consumers with [ model = NOTRUST ]"

SLIDER
200
508
387
541
liars_proportion
liars_proportion
0
100
0.0
1
1
NIL
HORIZONTAL

SWITCH
206
260
367
293
active-FIRE-IT-CR
active-FIRE-IT-CR
0
1
-1000

SLIDER
24
589
196
622
nb_iterations
nb_iterations
1
20
10.0
1
1
NIL
HORIZONTAL

TEXTBOX
145
563
295
582
Sensibility_analysis
15
0.0
0

MONITOR
210
590
383
635
Mean UG Performances
mean UGPerformances
4
1
11

MONITOR
59
634
162
679
current iteration
currIter
17
1
11

MONITOR
210
642
386
687
Std deviation UG Performances
standard-deviation UGPerformances
4
1
11

SLIDER
21
467
193
500
min-radius
min-radius
0
1
0.25
0.01
1
NIL
HORIZONTAL

SLIDER
23
509
195
542
max-radius
max-radius
0
1
0.75
0.01
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

This is an implementation of the trust and reputatioon model described in "Developing an Integrated Trust and Reputation Model for Open Multi-Agent Systems", Huynh, Trung Dong & R. Jennings, Nicholas & R. Shadbolt, Nigel. (2006). 13. 119-154. 10.1007/s10458-005-6825-4.


## HOW TO USE IT

setup: Initialize the evironnment and the agents wiith the chosen parameters

go: launch the simulation for NB_ROUNDS ticks. Each tick, the agents will choose a provider according to their trust model and interact with him if they want to use the service

go-once: launch the simulation for 1 tick

nb-providers-XXX: number of providers of type XXX to initialize in the environnment. The type of a provider is related with the quality of the service he provides, i.e. his performance

nb-consumers-per-group: number of consumer to initialize with each trust model selected (if we compare NOTRUST and FIRE-IT models and set nb-consumers-per-group=500, we'll have 1000 consumers)

active-XXX: whether or not we want to have consumers with trust model XXX. A consumer with NOTRUST  model will choose a provider randomly in his radius of operation, and the other models are FIRE model with 1
 2 or 3 trust components 

W_IT/W_WR/W_CR: weight of the components IT, WR and CR in the overall trust formula

min-radius/max-radius: the bounds in which the radius of operation of each agent will be initialized

liars-proportion: probability that an agent is a liar, i.e. willl act selfishly and provide false informations

initial-temperature: the "temperature" of an agent when he is initialized: set to decrease over time to decrease exploration (choosing an unknown provier) and increase the probability of exmploitation (choosing a known provider)

nb-iterations: how many times the program should run, used to sensitivity analysis



mean of reliability factors: the evolution of the reliablity of each FIRE component

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 3D 6.0.4
@#$#@#$#@
need-to-manually-make-preview-for-this-model
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
